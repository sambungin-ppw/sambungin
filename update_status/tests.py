from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import Status_Form
# Create your tests here.

class UpdateStatusTest(TestCase):
    def test_update_status_url_is_exists(self):
        response = Client().get('/update_status/')
        self.assertEqual(response.status_code, 200)

    def test_update_status_using_index_func(self):
        found = resolve('/update_status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(description='sedang mengerjakan tugas ppw nih')

        # Retrieving all available activity
        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_status_validation_for_blank_items(self):
        form = Status_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_model_can_create_new_post(self):
        # Creating a new activity
        new_status = Status.objects.create(description='mengerjakan update_status tugas 1 ppw')

        # Retrieving all available activity
        counting_all_available_post = Status.objects.all().count()
        self.assertEqual(counting_all_available_post, 1)

    def test_updatestatus_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update_status/add_status', {'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update_status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
