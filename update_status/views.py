from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

response = {}
# Create your views here.
def index(request):
	response['author'] = "Nurhikmah ilmi"
	todo = Status.objects.all()
	response['status'] = todo
	html = 'update_status.html'
	response['status_form'] = Status_Form
	return render(request, html, response)

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['description'] = request.POST['description']
		todo = Status(description=response['description'])
		todo.save()
		return HttpResponseRedirect('/update_status/')
	else:
		return HttpResponseRedirect('/update_status/')
