# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-13 11:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('add_friend', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='url',
            field=models.CharField(max_length=128),
        ),
    ]
