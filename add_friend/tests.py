from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Friend
from .forms import AddFriend_Form
from .views import index
# Create your tests here.

class Add_friendUnitTest(TestCase):

	def test_add_friend_url_is_exist(self):
		response = Client().get('/add_friend/')
		self.assertEqual(response.status_code,200)

	def test_add_friend_using_index_func(self):
		found = resolve('/add_friend/')
		self.assertEqual(found.func, index)

	def test_model_can_add_friend(self):
		#Creating a new activity
		new_friend = Friend.objects.create(name='Arief Wibowo',url='ppwku.herokuapp.com')

		#Retrieving all available activity
		counting_all_available_friends= Friend.objects.all().count()
		self.assertEqual(counting_all_available_friends,1)

	def test_friend_validation_for_blank_items(self):
		friends = AddFriend_Form(data={'name': '', 'url': ''})
		self.assertFalse(friends.is_valid())
		self.assertEqual(
			friends.errors['name'],
			["This field is required."]
		)

		self.assertEqual(
			friends.errors['url'],
			["This field is required."]
		)

	def test_add_friend_success_and_render_the_result(self):
		anonymous = 'Anonymous'
		response_friend = Client().post('/add_friend/add_friend', {'name': anonymous, 'url': anonymous})
		self.assertEqual(response_friend.status_code, 302)

		response = Client().get('/add_friend/')
		html_response = response.content.decode('utf8')
		self.assertIn(anonymous,html_response)
