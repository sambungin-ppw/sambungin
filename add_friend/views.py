from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import AddFriend_Form
from .models import Friend
# Create your views here.
response = {} 

def index(request):
	response['author'] = 'Muhammad Arief Wibowo'
	friend = Friend.objects.all()
	response['friends'] = friend
	html = 'add_friend/add_friend.html'
	response['friends_form'] = AddFriend_Form
	return render(request, html, response)

def add_friend(request):
	form = AddFriend_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		friends = Friend(name = request.POST['name'], url = request.POST['url'])
		friends.save() 
		return HttpResponseRedirect('/add_friend')