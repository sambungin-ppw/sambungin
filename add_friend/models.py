from django.db import models
from django.utils import timezone

class Friend(models.Model):
	
	def converTZ():
		return timezone.now() + timezone.timedelta(hours=0)
	
	name = models.CharField(max_length=27)
	url = models.CharField(max_length = 128)
	created_date = models.DateTimeField(default = converTZ)

	
# Create your models here.
