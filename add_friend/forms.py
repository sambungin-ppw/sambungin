from django import forms

class AddFriend_Form(forms.Form):
	error_messages = {
		'required': 'Tolong isi input ini',
		'invalid': 'Isi input dengan url',
	}
	attrs = {
		'class': 'form-control'
	}

	name = forms.CharField(label='Name',required=True, max_length=27,widget=forms.TextInput(attrs=attrs))
	url = forms.CharField(required=True, widget=forms.TextInput(attrs=attrs))

