# Tugas 1 | Kelompok 4 PPW - B 
**Pengembangan Aplikasi Web dengan TDD, Django, Models , HTML, CSS**

### Status Pipelines & Code Coverage
---
[![pipeline status](https://gitlab.com/sambungin-ppw/sambungin/badges/master/pipeline.svg)](https://gitlab.com/sambungin-ppw/sambungin/commits/master)

[![coverage report](https://gitlab.com/sambungin-ppw/sambungin/badges/master/coverage.svg)](https://gitlab.com/sambungin-ppw/sambungin/commits/master)

### Link to Heroku App
---
Link:
**[Heroku App](http://sambung-in.herokuapp.com/) : http://sambung-in.herokuapp.com/**

###  Tujuan
---
* Mengimplementasikan Disiplin Test Driven Development
* Mengimplementasikan konsep HTML dan Styling dengan CSS
* Mengimplementasikan Responsive Web Design
* Menggunakan Models untuk menyimpan data

### Nama-nama Anggota Kelompok
---
Tabel:

Nama Anggota          | NPM 
---                   |---
Muhammad Arief Wibowo | 1606837865
Nina Nursita Ramadhan | 1606834573
Nurhikmah Ilmi        | 1606918004
Winda Wijaya          | 1606836704
