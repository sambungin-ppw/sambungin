from django.shortcuts import render
from django.http import HttpResponseRedirect
from profile_page.models import Profile
from update_status.models import Status
from add_friend.models import Friend

response = {}
# Create your views here.
def index(request):
	html = 'dashboard.html'
	count_all_profiles = Profile.objects.count()
	if count_all_profiles == 0:
		profile = Profile.objects.create(
	            profile_pic='https://static.pexels.com/photos/247295/pexels-photo-247295.jpeg',
	            name='Katherine Wallace', birthday='November 30', gender='Female',
	            description='Stay positive!', email='mail@katewallace.com')
		profile.save()
	response['name']= Profile.objects.first().name
	response['profile_pic']= Profile.objects.first().profile_pic
	count = Status.objects.all().count()
	response['post_count']=count
	response['friend_count']=Friend.objects.all().count()
	if count > 0:
		response['description']=Status.objects.last().description
		response['time']=Status.objects.last().created_date
	else:
		response['description']="NO POSTS YET !!!"
		response['time']=""
	return render(request, html, response)
