from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from update_status.models import Status

class DashboardUnitTest(TestCase):
	def test_dashboard_url_is_exist(self):
		response = Client().get('/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_dashboard_using_index_func(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func, index)

	def test_dashboard_using_to_do_list_template(self):
		response = Client().get('/dashboard/')
		self.assertTemplateUsed(response, 'dashboard.html')

	def test_dashboard_status_is_exist(self):
		status= Status.objects.create(description="TEST")
		response = Client().get('/dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn("TEST", html_response)
