from django.db import models

# Create your models here.

class Profile(models.Model):
    profile_pic = models.URLField(default="https://maxcdn.icons8.com/Share/icon/Users//user_male_circle_filled1600.png")
    name = models.CharField(max_length=27)
    birthday = models.CharField(max_length=6, default='01 Jan')
    gender = models.CharField(max_length=10)
    expertise = models.CharField(max_length=27)
    # expertise = models.ManyToManyField('Expertise')
    description = models.CharField(max_length=27)
    email = models.EmailField()
# 
# class Expertise(models.Model):
#     expertise = models.CharField(max_length=27)
#
#     def __str__(self):
#         return str(self.expertise)
