from django.shortcuts import render
from .models import Profile

# Create your views here.

response = {}

def index(request):
    count_all_profiles = Profile.objects.count()

    # Create a new profile
    if count_all_profiles == 0:
        profile = Profile.objects.create(
            profile_pic='https://static.pexels.com/photos/247295/pexels-photo-247295.jpeg',
            name='Katherine Wallace', birthday='November 30', gender='Female', expertise='Walking, Breathing, Typing',
            description='Stay positive!', email='mail@katewallace.com')
        profile.save()

        # e1 = Expertise.objects.create(expertise='Fun Coding')
        # e1.save()
        # e2 = Expertise.objects.create(expertise='Modeling')
        # e2.save()
        # e3 = Expertise.objects.create(expertise='Taking Tests')
        # e3.save()
        #
        # profile.expertise.add(e1)
        # profile.expertise.add(e2)
        # profile.expertise.add(e3)
    else:
        profile = Profile.objects.first()

    # Inserting profile aspects into response
    response['profile_pic'] = profile.profile_pic
    response['name'] = profile.name
    response['birthday'] = profile.birthday
    response['gender'] = profile.gender
    # response['expertise'] = profile.expertise.all()
    response['description'] = profile.description
    response['email'] = profile.email

    # Specifying html
    html = 'profile_page.html'

    return render(request, html, response)
